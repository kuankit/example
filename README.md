# FIXUS
Fixus App is a visual tool for organising all your projects at work, on site, or anywhere in between.

With Fixus App you can:

**TACKLE TO-DO LISTS WITH EASE**  
Get more done with Fixus's customisable boards, lists, and cards. Go from project to action in seconds by creating task and assigning them across the board to follow your project's progress. Add checklists, labels, and due dates the way you see fit and give your projects the fuel they need to get across the finish line.

## Getting Started
This project is developed using [Flutter](https://flutter.dev/), an open-source mobile application development framework created by Google.

For help getting started with Flutter, view our online
[documentation](http://flutter.io/).

## Usage
Make sure you have Flutter installed on your local machine. For more instructions on how to install flutter, look [here](https://flutter.io/docs/get-started/install).

Clone the project
```
git clone https://gitlab.com/kuankit/fixus-app-1.git
cd fixus-app-1
```

Start running the app
```
flutter packages get
flutter run
```
**<span style="color:yellow">Warning</span>:** Please make sure you've ```GoogleService-Info.plist``` file store in your app directory for both ios and android to get rid of errors. You can refer to [Firebase setup](#firebase-setup) for more details.

## Firebase Setup 
The app is using firebase services to provide services such as push notification, crashlytics, auth login and etc. So make sure you've downloaded the **Firebase iOS config file** (```GoogleService-Info.plist```) in order to run the app.

1. Login to [Firebase Console](https://console.firebase.google.com) to obtain the ```GoogleService-Info.plist``` for both ios and android.
2. Drag the ```GoogleService-Info.plist``` file you just downloaded into iOS Runner subfolder.
    ```
    iOS
      |--Flutter
      |--Runner
      |   |--Assets.xcassets
      |   .
      |   .
      |   |--Info.plist
      |   |--GoogleService-Info.plist
    ```
    
    **<span style="color:red">Important</span>:** It is necessary to recover on the site of firebase the file IOS: GoogleService-Info.plist
    then open Xcode and drag the file directly to the application directory  

    **<span style="color:yellow">Warning</span>:** You have to go through Xcode otherwise it does not work.

3. Drag the ```GoogleService-Info.plist``` file to android/app subfolder.
    ```
    android
      |--app
      |   |
      |   |--src
      |   |--build.gradle
      |   |--GoogleService-Info.plist
    ```
4. If you've installed Flutter on your local machine, you can run the app by running 🚀:
    ```
    flutter packages get
    flutter run
    ```

## Pub Packages
The flutter development pub included:

Pub  | Description
---- | -------------
[Font Awesome Flutter](https://pub.dev/packages/font_awesome_flutter) | Font Awesome Icon pack available as set of Flutter Icons
[Flutter cupertino date picker](https://pub.dev/packages/flutter_cupertino_date_picker#-changelog-tab-) | Display DatePicker in iOS style
[Dio](https://pub.dev/packages/dio) | A powerful Http client for Dart
[RxDart](https://pub.dev/packages/rxdart) | ReactiveX api for asynchronous programming
[Shared preferences](https://pub.dev/packages/shared_preferences) | For reading and writing simple key-value pairs. Wraps NSUserDefaults on iOS and SharedPreferences on Android
[Image picker](https://pub.dev/packages/image_picker) | Selecting images from the Android and iOS image library and even taking new images with camera
[Flutter native image](https://pub.dev/packages/flutter_native_image) | Resize images and reduce their quality by compression
[Photo view](https://pub.dev/packages/photo_view) | Used to show interacive images and provides a gesture sensitive zoomable widget
[Cached network image](https://pub.dev/packages/cached_network_image) | Load and cache network images
[Location](https://pub.dev/packages/location) | For handle realtime location in iOS and Android
[Permission handler](https://pub.dev/packages/permission_handler) | API to request and check permissions
[Flutter inAppBrowser](https://pub.dev/packages/flutter_inappbrowser) | For open an in-app browser window
[Firebase core](https://pub.dev/packages/firebase_core) | To use the Firebase Core API, which enables connecting to multiple Firebase apps
[Firebase crashlytics](https://pub.dev/packages/firebase_crashlytics) | It reports uncaught errors to the Firebase console
[Firebase auth](https://pub.dev/packages/firebase_auth) | Enabling Android and iOS authentication using passwords, phone numbers and identity providers like Google, Facebook and Twitter
[Cloud firestore](https://pub.dev/packages/cloud_firestore) | Cloud-hosted, noSQL database with live synchronization and offline
[Firebase messaging](https://pub.dev/packages/firebase_messaging) | Receive and process push notifications as well as data messages on Android and iOS
[Pull to refresh](https://pub.dev/packages/pull_to_refresh) | Scroll component for drop-down refresh and pull up load
[Multi image picker](https://pub.dev/packages/multi_image_picker) | To display multi image picker on iOS and Android.
[Image downloader](https://pub.dev/packages/image_downloader) | Download images on the Internet and saves them on device
[Flushbar](https://pub.dev/packages/flushbar) | Customization for notifying user, such as toasts and snackbars
[Flutter image compress](https://pub.dev/packages/flutter_image_compress) | Compress image with native code(objc kotlin), it's faster.
[Clipboard manager](https://pub.dev/packages/clipboard_manager) | For copy text to clipboard
[File picker](https://pub.dev/packages/file_picker) | To use a native file explorer to pick single or multiple absolute file paths, with extension filtering support
[Flutter launcher icons](https://pub.dev/packages/flutter_launcher_icons) | **DevDependency** package that simplifies the task of updating flutter's app launcher icon